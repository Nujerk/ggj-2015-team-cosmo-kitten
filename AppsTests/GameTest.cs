﻿using System;
using System.Collections.Generic;
using System.Linq;
using AssemblyCSharp;
using Assets.Scripts.GenericBehavior.Controler;
using Assets.Scripts.GenericBehavior.Entity;
using Assets.Scripts.GenericBehavior.Enum;

namespace AppsTests
{
    public class GameTest
    {
        private const int NUM_OF_LINE = 13;
        private const string WRONG_CHOICE_FR = "Mauvaise Option";
        private const string WRONG_CHOICE_EN = "Wrong Option";
        private static GameTest _instance;
        private QTEDifficulty _difficulty;
        private LangEnum _lang = LangEnum.English;
        private string _name;

        private GameTest()
        {
        }

        public static GameTest Instance
        {
            get { return _instance ?? (_instance = new GameTest()); }
        }

        private bool IsFrench
        {
            get { return _lang == LangEnum.French; }
        }


        public void Start()
        {
            Console.Clear();
            Console.WriteLine("=== Game");
            Console.WriteLine(string.Empty);
            Console.Write("Your Name ? ");
            _name = Console.ReadLine().Trim();
            Console.Write("Your Language ? (fr/en) ");
            _lang = Console.ReadLine().ToLower().Substring(0, 1) == "f" ? LangEnum.French : LangEnum.English;
            Console.Write("Qte Dificulty ? (1/2/3) ");
            switch (ReadFirstLetter())
            {
                case "1":
                    _difficulty = QTEDifficulty.EASY;
                    break;
                case "2":
                    _difficulty = QTEDifficulty.HARD;
                    break;
                case "3":
                    _difficulty = QTEDifficulty.MEDIUM;
                    break;
                default:
                    _difficulty = QTEDifficulty.EASY;
                    break;
            }

            PrintRoom(MapGenerator.Instance.Build());
        }


        private void PrintRoom(Room room, bool nextRoom = false, string sysMess = "")
        {
            if (room.EventRoom.EventType == EventTypeEnum.Empty || nextRoom)
                PrintRoomChoice(room, sysMess);
            else
                PrintEvent(room, sysMess);
        }

        private void PrintEvent(Room room, string sysMess = "")
        {
            var list = new List<string>();
            list.Add(string.Format("[ref. {0}] [{1}]", room.EventRoom.Id.ToString("00"), room.EventRoom.EventType));
            list.AddRange(room.EventRoom.StartMessage.GetMessage(_lang));
            while (list.Count() != NUM_OF_LINE)
                list.Add(string.Empty);


            if (room.EventRoom.EventType == EventTypeEnum.Qte)
            {
                list[6] = "Qte: ";
                var qte = new QTE(room, room.EventRoom.QteTape, _difficulty);
                foreach (string key in qte.GetKeys())
                    list[6] += string.Format("[{0}]", key);
            }

            list[8] = "Options:";
            list[9] = "5: Next";
            list[10] = "0: Quit";
            list[11] = string.Empty;
            list[12] = "Your choice ? ";

            switch (PrintScreen(room, sysMess, list.ToArray()))
            {
                case "5":
                    PrintRoomChoice(room);
                    break;
                case "0":
                    break;
                default:
                    PrintEvent(room, IsFrench ? WRONG_CHOICE_FR : WRONG_CHOICE_EN);
                    break;
            }
        }

        private void PrintRoomChoice(Room room, string sysMess = "")
        {
            var list = new List<string>();

            list.Add("Where we go now ?");
            var options = new Dictionary<byte, LocationEnum>();

            if (room.Rooms[LocationEnum.Bottom] != null)
            {
                options.Add(2, LocationEnum.Bottom);
                list.Add("2: Bottom");
            }

            if (room.Rooms[LocationEnum.Left] != null)
            {
                options.Add(4, LocationEnum.Left);
                list.Add("4: Left");
            }

            if (room.Rooms[LocationEnum.Right] != null)
            {
                options.Add(6, LocationEnum.Right);
                list.Add("6: Right");
            }

            if (room.Rooms[LocationEnum.Top] != null)
            {
                options.Add(8, LocationEnum.Top);
                list.Add("8: Top");
            }

            while (list.Count() != NUM_OF_LINE)
                list.Add(string.Empty);

            list[12] = "Your choice ? ";

            string result = PrintScreen(room, sysMess, list.ToArray());
            byte num;
            if (byte.TryParse(result, out num))
            {
                if (options.ContainsKey(num))
                    PrintRoom(room.Rooms[options[num]]);
            }
            PrintRoomChoice(room, IsFrench ? WRONG_CHOICE_FR : WRONG_CHOICE_EN);
        }

        private string PrintScreen(Room room, string sysMess, params string[] lines)
        {
            Console.Clear();

            Console.WriteLine("=== Game            | {0}", !string.IsNullOrWhiteSpace(sysMess) ? "Info.:" : string.Empty);
            Console.WriteLine("                    | {0}", sysMess);
            Console.WriteLine("{1}: {0}", _name, IsFrench ? "Util." : "User");
            Console.WriteLine();
            Console.WriteLine("==================  | {0}", GetLine(0, lines));
            Console.WriteLine("==    Level {0}   ==  | {1}", room.Coordinate.Level, GetLine(1, lines));
            Console.WriteLine("==================  | {0}", GetLine(2, lines));
            Console.WriteLine("                    | {0}", GetLine(3, lines));
            Console.WriteLine("   .----{0}----.    | {1}", Horizontal(room, room.Rooms[LocationEnum.Top]),
                GetLine(4, lines));
            Console.WriteLine("   |           |    | {0}", GetLine(5, lines));
            Console.WriteLine("   |           |    | {0}", GetLine(6, lines));
            Console.WriteLine("  {0}         {1}   | {2}",
                Vertical(room, room.Rooms[LocationEnum.Left]),
                Vertical(room, room.Rooms[LocationEnum.Right]), GetLine(7, lines));
            Console.WriteLine("   |           |    | {0}", GetLine(8, lines));
            Console.WriteLine("   |           |    | {0}", GetLine(9, lines));
            Console.WriteLine("   '----{0}----'    | {1}", Horizontal(room, room.Rooms[LocationEnum.Bottom]),
                GetLine(10, lines));
            Console.WriteLine("                    | {0}", GetLine(11, lines));
            Console.Write("==================  | {0}", GetLine(12, lines));
            return ReadFirstLetter();
        }

        private string ReadFirstLetter()
        {
            string word = Console.ReadLine().ToLower();
            if (word.Length < 1) return string.Empty;
            return word.Substring(0, 1);
        }


        private string GetLine(int index, params string[] lines)
        {
            return index > (lines.Count() - 1) ? string.Empty : lines[index];
        }


        private string Vertical(Room current, Room room)
        {
            return Alanyse(current, room, " | ");
        }


        private string Alanyse(Room current, Room room, string wall)
        {
            if (room == null)
                return wall;

            if (current.IsExit && room.Coordinate.Level != current.Coordinate.Level)
                return "[S]";

            if (current.IsEntry && room.Coordinate.Level != current.Coordinate.Level)
                return "[E]";

            return "[ ]";
        }

        private string Horizontal(Room current, Room room)
        {
            return Alanyse(current, room, "---");
        }
    }
}