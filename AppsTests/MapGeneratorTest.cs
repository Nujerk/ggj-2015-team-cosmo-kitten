﻿using System;
using System.Text;
using Assets.Scripts.GenericBehavior.Controler;
using Assets.Scripts.GenericBehavior.Entity;
using Assets.Scripts.GenericBehavior.Enum;

namespace AppsTests
{
    public class MapGeneratorTest
    {
        private static MapGeneratorTest _instance;

        private MapGeneratorTest()
        {
        }

        public static MapGeneratorTest Instance
        {
            get { return _instance ?? (_instance = new MapGeneratorTest()); }
        }


        public void Start()
        {
            do
            {
                Console.Clear();
                MapGenerator.Instance.Build();
                byte _lastLevel = 0;
                byte _lastY = 1;

                var line1 = new StringBuilder();
                var line2 = new StringBuilder();
                var line3 = new StringBuilder();
                var line4 = new StringBuilder();
                var line5 = new StringBuilder();
                foreach (Room r in MapGenerator.Instance.Collection)
                {
                    if (r.Coordinate.Y != _lastY ||
                        (r.Coordinate.Level == 2 && r.Coordinate.X == 1 && r.Coordinate.Y == 1))
                    {
                        _lastY = r.Coordinate.Y;
                        Console.WriteLine(line1.ToString());
                        Console.WriteLine(line2.ToString());
                        Console.WriteLine(line3.ToString());
                        Console.WriteLine(line4.ToString());
                        Console.WriteLine(line5.ToString());
                        line1 = new StringBuilder();
                        line2 = new StringBuilder();
                        line3 = new StringBuilder();
                        line4 = new StringBuilder();
                        line5 = new StringBuilder();
                    }

                    if (r.Coordinate.Level != _lastLevel)
                    {
                        _lastLevel = r.Coordinate.Level;
                        Console.WriteLine("============={1}LVL {0}:{1}=============", _lastLevel,
                            Environment.NewLine);
                    }

                    line1.Append(string.Format(" .---{0}---. ", Horizontal(r, r.Rooms[LocationEnum.Top])));
                    line2.Append(string.Format(" |   X:{0}   | ", r.Coordinate.X));
                    line3.Append(string.Format("{0}  Y:{2}  {1}",
                        Vertical(r, r.Rooms[LocationEnum.Left]),
                        Vertical(r, r.Rooms[LocationEnum.Right]),
                        r.Coordinate.Y));
                    line4.Append(string.Format(" |   R:{0}   | ", GetRoomType(r.EventRoom.EventType)));
                    line5.Append(string.Format(" '---{0}---' ", Horizontal(r, r.Rooms[LocationEnum.Bottom])));
                }

                Console.WriteLine(line1.ToString());
                Console.WriteLine(line2.ToString());
                Console.WriteLine(line3.ToString());
                Console.WriteLine(line4.ToString());
                Console.WriteLine(line5.ToString());

                Console.WriteLine("Legend:");
                Console.WriteLine("' ' is Empty, '?' is Enigma, 'E' is Entry, 'J' is Jail");
                Console.WriteLine("'M' is Meeting, 'D' is Mortal, 'Q' is Qte, 'T' is Treasure");

                Console.Write("Regenerate ? (y/n) : ");
            } while (Console.ReadLine().Substring(0, 1).ToLower() == "y");
        }


        public string GetRoomType(EventTypeEnum type)
        {
            switch (type)
            {
                case EventTypeEnum.Empty:
                    return " ";
                case EventTypeEnum.Enigma:
                    return "?";
                case EventTypeEnum.Entry:
                    return "E";
                case EventTypeEnum.Jail:
                    return "J";
                case EventTypeEnum.Meeting:
                    return "M";
                case EventTypeEnum.Mortal:
                    return "D";
                case EventTypeEnum.Qte:
                    return "Q";
                case EventTypeEnum.Treasure:
                    return "T";
                default:
                    return "_";
            }
        }

        private string Vertical(Room current, Room room)
        {
            return Alanyse(current, room, " | ");
        }


        private string Alanyse(Room current, Room room, string wall)
        {
            if (room == null)
                return wall;

            if (current.IsExit && room.Coordinate.Level != current.Coordinate.Level)
                return "[S]";

            if (current.IsEntry && room.Coordinate.Level != current.Coordinate.Level)
                return "[E]";

            return "[ ]";
        }

        private string Horizontal(Room current, Room room)
        {
            return Alanyse(current, room, "---");
        }
    }
}