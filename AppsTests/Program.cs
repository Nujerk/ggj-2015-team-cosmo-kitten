﻿using System;

namespace AppsTests
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string request = string.Empty;
            do
            {
                Console.Clear();
                Console.WriteLine("=== Menu");
                Console.WriteLine("What do you want to do ?");
                Console.WriteLine("1: Run Map Generator");
                Console.WriteLine("2: Wanna play with Dora the Explorer");
                Console.WriteLine(string.Empty);
                Console.WriteLine("q: Quit");

                Console.WriteLine(string.Empty);
                Console.Write("Option ? ");
                request = Console.ReadLine().ToLower().Substring(0, 1);

                switch (request)
                {
                    case "1":
                        MapGeneratorTest.Instance.Start();
                        break;
                    case "2":
                        GameTest.Instance.Start();
                        break;
                    case "q":
                        return;
                }
            } while (true);
        }
    }
}