﻿using System.Collections.Generic;
using Assets.Scripts.GenericBehavior.Enum;

namespace Assets.Scripts.GenericBehavior.Entity
{
    public class Room
    {
        private Dictionary<LocationEnum, Room> _rooms;

        public Room()
        {
            ActiveTrigger = true;
            //var toto = Coordinate.X ==1 && Coordinate.Y == 1 && Coordinate.Level == 1;
        }

        public Dictionary<LocationEnum, Room> Rooms
        {
            get { return _rooms ?? (_rooms = GetInit()); }
        }

        public EventRoom EventRoom { get; set; }

        public Coordinate Coordinate { get; set; }

        public bool IsActive { get; set; }

        public bool IsEntry { get; set; }
        public bool IsExit { get; set; }

        public string[] StartingMessage { get; set; }

        public string[] EndingMessage { get; set; }


        public bool ActiveTrigger { get; set; }

        public bool Locked { get; set; }

        private Dictionary<LocationEnum, Room> GetInit()
        {
            var dico = new Dictionary<LocationEnum, Room>
            {
                {LocationEnum.Top, null},
                {LocationEnum.Bottom, null},
                {LocationEnum.Right, null},
                {LocationEnum.Left, null}
            };

            return dico;
        }
    }
}