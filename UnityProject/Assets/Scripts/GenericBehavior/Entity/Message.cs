﻿using System.Collections.Generic;
using Assets.Scripts.GenericBehavior.Enum;

namespace Assets.Scripts.GenericBehavior.Entity
{
    public class Message
    {
        private readonly Dictionary<LangEnum, List<string>> _message = new Dictionary<LangEnum, List<string>>();

        public Message()
        {
            _message = new Dictionary<LangEnum, List<string>>();
            _message.Add(LangEnum.French, new List<string>());
            _message.Add(LangEnum.English, new List<string>());
        }


        public List<string> GetMessage(LangEnum lang)
        {
            return _message[lang];
        }

        public void SetMessage(LangEnum lang, string[] messages)
        {
            _message[lang].Clear();
            _message[lang].AddRange(messages);
        }

        public void SetMessage(string[] french, string[] english)
        {
            SetMessage(LangEnum.French, french);
            SetMessage(LangEnum.English, english);
        }
    }
}