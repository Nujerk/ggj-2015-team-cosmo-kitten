using System;

namespace Assets.Scripts.GenericBehavior.Entity
{
    public class Coordinate
    {
        public Coordinate(Byte x, Byte y, Byte level)
        {
            X = x;
            Y = y;
            Level = level;
        }

        public byte X { get; set; }

        public byte Y { get; set; }

        public byte Level { get; set; }
    }
}