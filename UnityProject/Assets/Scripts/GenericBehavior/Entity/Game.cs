﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Assets.Scripts.GenericBehavior.Controler;
using Assets.Scripts.GenericBehavior.Enum;
using System.Linq;
using AssemblyCSharp;

public class Game : MonoBehaviour {

	private const int MAX_ALLOWED_TIME = 300;

	private static 	float 		s_LastNotifTime;
	private static 	GameObject 	s_Notif;
	private static 	QTE 		s_QTE;
	public 	static 	GameObject 	s_MiniMap;
	public 	static	float		s_LastDeadTime;

	public static Group s_InitialGroup;
	public static Group s_FirstGroup;
	public static Group s_SecondGroup;
	public static Group s_CurrentGroup;

	public static bool s_HasChest = false;
	public static bool s_OutOfHell = false;


	public 	Image 	m_MiniMapButton;
	private bool	m_GameOver = false;

	// Use this for initialization
	void Start () {
		s_Notif = GameObject.Find("/GameUI/MainPanel/GroupPanel/GroupPanelAlert");
		s_Notif.SetActive(false);

		s_InitialGroup = new Group(4);
		s_InitialGroup.GenerateUnits();

		Unit.s_UnitPictures = new Sprite[8];
		Unit.s_UnitPictures[0] = Resources.Load<Sprite>("indybones");
		Unit.s_UnitPictures[1] = Resources.Load<Sprite>("lara_soft");
		Unit.s_UnitPictures[2] = Resources.Load<Sprite>("bobmurene");
		Unit.s_UnitPictures[3] = Resources.Load<Sprite>("sydney_ox");
		Unit.s_UnitPictures[4] = Resources.Load<Sprite>("indybones_dead");
		Unit.s_UnitPictures[5] = Resources.Load<Sprite>("lara_soft_dead");
		Unit.s_UnitPictures[6] = Resources.Load<Sprite>("bobmurene_dead");
		Unit.s_UnitPictures[7] = Resources.Load<Sprite>("sydney_ox_dead");



		s_InitialGroup.m_Room = MapGenerator.Instance.Build();

		s_FirstGroup = new Group (2);
		s_FirstGroup.AddUnit (s_InitialGroup.m_Units [0]);
		s_FirstGroup.AddUnit (s_InitialGroup.m_Units [1]);
		s_FirstGroup.m_Room = s_InitialGroup.m_Room;
		s_SecondGroup = new Group (2);
		s_SecondGroup.AddUnit (s_InitialGroup.m_Units [2]);
		s_SecondGroup.AddUnit (s_InitialGroup.m_Units [3]);
		s_SecondGroup.m_Room = s_InitialGroup.m_Room;

		s_MiniMap = GameObject.Find("/GameUI/MainPanel/GroupPanel/GroupMapPanel");
		var l_FirstButton = (Image) Instantiate(m_MiniMapButton);
		l_FirstButton.GetComponent<MiniMapButton>().m_Room = s_InitialGroup.m_Room;
		l_FirstButton.transform.SetParent(s_MiniMap.transform);
		s_CurrentGroup = s_FirstGroup;

	}
	
	// Update is called once per frame
	void Update () {

		// Game over, stop computing
		if (m_GameOver)
			return;

		// Check if all the players are out of the temple
		if (s_OutOfHell)
			return;

		// Check if timer is elapsed
		if (Time.time > MAX_ALLOWED_TIME) {
			m_GameOver = true;
			EndGame();
			return;
		}


		// Check if group is dead.
		bool l_Alive = false;
		Unit l_CurrentUnit;
		for (var i = 0; i < s_InitialGroup.m_NbUnit; i ++) {
			l_CurrentUnit = s_InitialGroup.m_Units[i];
			l_Alive = l_CurrentUnit.m_Alive;
			if(l_Alive)
			{
				break;
			}
		}
		if (!l_Alive) {
			m_GameOver = true;
			EndGame();
		}

		// Display final countdown
		int l_RemainingTime = Mathf.FloorToInt(MAX_ALLOWED_TIME - Time.time);
		if (l_RemainingTime <= 60)
			GameObject.Find ("/GameUI/MainPanel/Chrono/ChronoText").GetComponent<Text> ().color = Color.red;

		// If last notif displayed since 3s, hide it
		if (Time.time - s_LastNotifTime > 3)
			HideNotif ();

		// Update timer
		int l_Seconds = l_RemainingTime % 60;
		string l_SecondsText = (l_Seconds < 10 ? "0" : "") + l_Seconds;
		int l_Minutes = Mathf.FloorToInt(l_RemainingTime / 60);
		string l_MinutesText = (l_Minutes < 10 ? "0" : "") + l_Minutes;
		GameObject.Find("/GameUI/MainPanel/Chrono/ChronoText").GetComponent<Text>().text = "" + l_MinutesText + ":" + l_SecondsText;


		// Dead message handler
		if (Time.time - s_LastDeadTime > 3) {
			GameObject.Find ("/GameUI/MainPanel/RoomPanel/PanelTop/Dead").GetComponent<Image>().enabled = false;
			GameObject.Find ("/GameUI/MainPanel/RoomPanel/PanelBottom/Dead").GetComponent<Image>().enabled = false;
		}

		// Handle current QTE
		if (s_QTE != null) {
			if(s_QTE.IsElapsed())
				s_QTE.Fail();

			string l_KeyPressed = Input.inputString;
			if(l_KeyPressed != null)
			{
				if(s_QTE.CheckKey(l_KeyPressed))
				{
					if(!s_QTE.NextStep())
					{
						s_QTE.Succeed();
						s_QTE = null;
					}
				}
				else{
					s_QTE.Fail();
				}
			}
		}
	}

	static public void Notif(string message)
	{
		s_Notif.SetActive (true);
		s_Notif.GetComponent<Text> ().text = message;
		s_LastNotifTime = Time.time;
	}

	static private void HideNotif()
	{
		s_Notif.SetActive(false);
	}

	static public void startQte(QTE p_QTE, Group p_CurrentGroup)
	{
		s_QTE = p_QTE;

		GameObject l_RoomTop = GameObject.Find("/GameUI/MainPanel/RoomPanel/PanelTop");
		GameObject l_RoomBottom = GameObject.Find("/GameUI/MainPanel/RoomPanel/PanelBottom");
		if (Game.s_FirstGroup.m_Room == Game.s_SecondGroup.m_Room) {
			if(p_CurrentGroup == Game.s_FirstGroup){
				l_RoomTop.GetComponent<Image>().color = Color.gray;
				l_RoomTop.GetComponent<RoomEngine>().IsActive = false;
			}
			else{
				l_RoomBottom.GetComponent<Image>().color = Color.gray;
				l_RoomBottom.GetComponent<RoomEngine>().IsActive = false;
			}
		}

		s_QTE.Init ();
	}

	static public void EndGame(){

	}
}
