//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.18444
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using Assets.Scripts.GenericBehavior.Enum;


namespace Assets.Scripts.GenericBehavior.Entity
{
		public class EventRoom
		{
			public EventRoom (byte id, EventTypeEnum e)
			{
				EventType = e;
			    Id = id;

                StartMessage = new Message();
                EndMessage = new Message();
			}


            public byte Id { get; private set; }

            public bool Floor { get; set; }

            public bool Final { get; set; }

            public bool Transition { get; set; }

            public bool Skip { get; set; }

            public byte QteTape{ get; set; }

            public bool Lock { get; set; }
            public bool Disable { get; set; }

		    public Message StartMessage { get; set; }
            public Message EndMessage { get; set; }
            
			public EventTypeEnum EventType { get; set; }
		}
}

