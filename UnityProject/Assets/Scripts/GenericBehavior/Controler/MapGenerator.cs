﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GenericBehavior.Entity;
using Assets.Scripts.GenericBehavior.Enum;
using UnityEngine;

namespace Assets.Scripts.GenericBehavior.Controler
{
    public class MapGenerator
    {
        private const byte MaxLvlNb = 4;
        private static MapGenerator _instance;
        private readonly System.Random _random;
        public List<EventRoom> _eventsRoom = new List<EventRoom>();

        private MapGenerator()
        {
			_random = new System.Random();
            Collection = new List<Room>();
        }

        public static MapGenerator Instance
        {
            get { return _instance ?? (_instance = new MapGenerator()); }
        }

        public List<Room> Collection { get; set; }

        public Room Build()
        {
            for (int i = 0; i < 8; i++)
            {
                _eventsRoom.Add(DataCollection.EventRoomCollection.Instance.Get(EventTypeEnum.Qte, final: false));
                DataCollection.EventRoomCollection.Instance.DisableEvent(_eventsRoom.Last());
            }
            for (int i = 0; i < 9; i++)
            {
                _eventsRoom.Add(DataCollection.EventRoomCollection.Instance.Get(EventTypeEnum.Meeting));
                DataCollection.EventRoomCollection.Instance.DisableEvent(_eventsRoom.Last());
            }

            Collection.Clear();
            BuildLevels(1, MaxLvlNb);
            BuildTreasureRoom(MaxLvlNb + 1);
            return GetRoom(1, 1, 1);
        }

        private void BuildTreasureRoom(byte level)
        {
            Collection.Add(new Room
            {
                EventRoom = DataCollection.EventRoomCollection.Instance.TreasureRoom,
                Coordinate = new Coordinate(1, 1, level),
                IsEntry = true,
            });
            CreateLinkExitEntry(level);
        }


        public Room GetRoom(Coordinate coor)
        {
            return GetRoom(coor.X, coor.Y, coor.Level);
        }

        public Room GetRoom(int x, int y, int level)
        {
            return
                Collection.FirstOrDefault(r => r.Coordinate.Level == level && r.Coordinate.X == x && r.Coordinate.Y == y);
        }


        private IEnumerable<Room> RoomsOnSide(byte level)
        {
            return Collection.Where(r => r.Coordinate.Level == level &&
                                         !r.IsEntry &&
                                         !r.IsExit &&
                                         (r.Coordinate.X == 1 ||
                                          r.Coordinate.Y == 1 ||
                                          r.Coordinate.X == r.Coordinate.Level ||
                                          r.Coordinate.Y == r.Coordinate.Level));
        }

        private void BuildLevels(byte level, byte max)
        {
            if (level > max) return;

            // Create All Rooms
            for (int i = 1; i <= (level*level); i++)
            {
                Collection.Add(new Room
                {
                    EventRoom = DataCollection.EventRoomCollection.Instance.EmptyRoom,
                    Coordinate = GetCoordinate(level, i),
                });
            }

            // Link All Room each others
            foreach (Room room in Collection)
            {
                if (room.Rooms[LocationEnum.Left] == null)
                    room.Rooms[LocationEnum.Left] = GetRoom(room.Coordinate.X - 1, room.Coordinate.Y,
                        room.Coordinate.Level);
                if (room.Rooms[LocationEnum.Top] == null)
                room.Rooms[LocationEnum.Top] = GetRoom(room.Coordinate.X, room.Coordinate.Y - 1, room.Coordinate.Level);
                if (room.Rooms[LocationEnum.Right] == null)
                room.Rooms[LocationEnum.Right] = GetRoom(room.Coordinate.X + 1, room.Coordinate.Y, room.Coordinate.Level);
                if (room.Rooms[LocationEnum.Bottom] == null)
                room.Rooms[LocationEnum.Bottom] = GetRoom(room.Coordinate.X, room.Coordinate.Y + 1,
                    room.Coordinate.Level);
            }

            if (!RoomsOnSide(level).Any())
                throw new Exception("BuildLevels none available rooms on the side.");

            var exitRoom = CreateExit(level, max);
            if (level != 1) 
                CreateEntry(level, exitRoom);

            CreateLinkExitEntry(level);

            if (level == 2)
            {
                // Level 2
                for (var i = 0; i < 2; i++)
                    GetEmptyRoom(level).EventRoom = GetRandomEvent();
            }
            else if (level == 3)
            {
                // Level 3
                GetEmptyRoom(level).EventRoom = DataCollection.EventRoomCollection.Instance.Get(EventTypeEnum.Jail);
                GetEmptyRoom(level).EventRoom = DataCollection.EventRoomCollection.Instance.Get(EventTypeEnum.Enigma, floor: true);
                for (var i = 0; i < 5; i++)
                    GetEmptyRoom(level).EventRoom = GetRandomEvent();
            }
            else if (level == 4)
            {
                // Lebel 4
                GetEmptyRoom(level).EventRoom = DataCollection.EventRoomCollection.Instance.Get(EventTypeEnum.Jail);
                GetEmptyRoom(level).EventRoom = DataCollection.EventRoomCollection.Instance.Get(EventTypeEnum.Enigma, floor: true);
                GetEmptyRoom(level).EventRoom = DataCollection.EventRoomCollection.Instance.Get(EventTypeEnum.Enigma, floor: true);
                GetEmptyRoom(level).EventRoom = DataCollection.EventRoomCollection.Instance.Get(EventTypeEnum.Mortal);
                for (var i = 0; i < 10; i++)
                    GetEmptyRoom(level).EventRoom = GetRandomEvent();
            }

            BuildLevels(++level, max);
        }


        private Room CreateEntry(byte level, Room exitRoom)
        {
            // Exit Room must be min at 2 Room near Entry Room
            var available = AvailableRoomNearEntry(RoomsOnSide(level), exitRoom);

            // Define Entry Room
            var entryRoom = available.ElementAt(_random.Next(available.Count()));
            entryRoom.IsEntry = true;
            // Special Event
            entryRoom.EventRoom = DataCollection.EventRoomCollection.Instance.Get(EventTypeEnum.Entry);
            return entryRoom;
        }

        private Room CreateExit(byte level, byte maxLevel)
        {
            // Define Exit Room
            var exitRoom = RoomsOnSide(level).Count() == 1
                ? RoomsOnSide(level).ElementAt(0)
                : RoomsOnSide(level).ElementAt(_random.Next(RoomsOnSide(level).Count()));
            exitRoom.IsExit = true;
            // Special Event
            exitRoom.EventRoom = DataCollection.EventRoomCollection.Instance.Get(EventTypeEnum.Enigma, transit: true, final: level == maxLevel);
            return exitRoom;
        }

        private void CreateLinkExitEntry(byte level)
        {
            // Link Entry & Exit Room
            var entryRoom = Collection.FirstOrDefault(r => r.Coordinate.Level == level && r.IsEntry);
            var previousExitRoom = Collection.FirstOrDefault(r => r.Coordinate.Level == level - 1 && r.IsExit);

            if(entryRoom == null || previousExitRoom == null) return;
            entryRoom.Rooms[GetEmptyWallOutside(entryRoom)] = previousExitRoom;
            previousExitRoom.Rooms[GetEmptyWallOutside(previousExitRoom)] = entryRoom;
        }

        
        public LocationEnum GetEmptyWallOutside(Room room)
        {
            var list = new List<LocationEnum>();
            if(room.Rooms[LocationEnum.Bottom] == null)
                list.Add(LocationEnum.Bottom);
            if(room.Rooms[LocationEnum.Right] == null)
                list.Add(LocationEnum.Right);
            if(room.Rooms[LocationEnum.Left] == null)
                list.Add(LocationEnum.Left);
            if(room.Rooms[LocationEnum.Top] == null)
                list.Add(LocationEnum.Top);


            if (list.Contains(LocationEnum.Top) && room.Coordinate.Y != 1)
                list.Remove(LocationEnum.Top);

            if (list.Contains(LocationEnum.Left) && room.Coordinate.X != 1)
                list.Remove(LocationEnum.Left);

            if (list.Contains(LocationEnum.Bottom) && room.Coordinate.Y != room.Coordinate.Level)
                list.Remove(LocationEnum.Bottom);

            if (list.Contains(LocationEnum.Right) && room.Coordinate.X != room.Coordinate.Level)
                list.Remove(LocationEnum.Right);

            if (!list.Any())
                throw new Exception("GetEmptyWallOutside Undentify");

            if (list.Count() == 1)
                return list[0];

            return list[_random.Next(list.Count())];

        }

        private EventRoom GetRandomEvent()
        {
            if (!_eventsRoom.Any()) return DataCollection.EventRoomCollection.Instance.EmptyRoom;
            var e = _eventsRoom.Count() == 1 ? _eventsRoom.First() : _eventsRoom[_random.Next(_eventsRoom.Count())];
            _eventsRoom.Remove(e);
            return e;
        }

        private Room GetEmptyRoom(byte level)
        {
            IEnumerable<Room> elements =
                Collection.Where(r => r.EventRoom.EventType == EventTypeEnum.Empty && r.Coordinate.Level == level);
            return elements.ElementAt(_random.Next(elements.Count()));
        }

        private List<Room> AvailableRoomNearEntry(IEnumerable<Room> enumerable, Room entry)
        {
            var collection = enumerable.ToList();
            collection.Remove(entry);

            Room room = GetRoom(entry.Coordinate.X - 1, entry.Coordinate.Y, entry.Coordinate.Level);
            if (room != null)
                collection.Remove(room);

            room = GetRoom(entry.Coordinate.X + 1, entry.Coordinate.Y, entry.Coordinate.Level);
            if (room != null)
                collection.Remove(room);

            room = GetRoom(entry.Coordinate.X, entry.Coordinate.Y - 1, entry.Coordinate.Level);
            if (room != null)
                collection.Remove(room);

            room = GetRoom(entry.Coordinate.X, entry.Coordinate.Y + 1, entry.Coordinate.Level);
            if (room != null)
                collection.Remove(room);


            return collection;
        }

        private Coordinate GetCoordinate(byte level, int index)
        {
            var x = index%level;
            if (x == 0)
                x = level;
            var y = ((index - 1)/level) + 1;

             return new Coordinate((byte) x, (byte) y, level);
        }
    }
}