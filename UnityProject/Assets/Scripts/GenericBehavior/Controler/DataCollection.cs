﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GenericBehavior.Entity;
using Assets.Scripts.GenericBehavior.Enum;

namespace Assets.Scripts.GenericBehavior.Controler
{
    public class DataCollection
    {
        public class EventRoomCollection
        {
            private static EventRoomCollection _instance;
            private readonly Dictionary<EventRoom, bool> _events = new Dictionary<EventRoom, bool>();

            private readonly Random _random = new Random();


            private EventRoomCollection()
            {
                EventRoom e;

                #region Enigma

                // Code 0001
                e = Add(1, EventTypeEnum.Enigma, floor: true);
                AddMess(e, true, false, "Passez sur toutes les dalles sans repasser au même endroit.");
                AddMess(e, false, false, "Walk on every tiles, but only once!");
                // Code 0002
                e = Add(2, EventTypeEnum.Enigma, floor: true);
                AddMess(e, true, false, "Passez sur toutes les dalles sans repasser au même endroit.");
                AddMess(e, false, false, "Walk on every tiles, but only once!");
                // Code 0003
                e = Add(3, EventTypeEnum.Enigma, floor: true, disable: true);
                AddMess(e, true, false, "Certaines dalles semblent détachées du sol.",
                    "Les replacer ouvrira peut être la porte ?");
                AddMess(e, false, false, "A few stones seem to be riped of the ground.",
                    "May be putting them back will open the trapdoor.");
                AddMess(e, true, true, "Vous trouvez un trésor sous la trappe !");
                AddMess(e, false, true, "You find a treasure hiding beneath the trapdoor.");
                // Code 0004
                e = Add(4, EventTypeEnum.Enigma, floor: true, disable: true);
                AddMess(e, true, false, "Certaines dalles semblent détachées du sol.",
                    "Les replacer ouvrira peut être la porte ?");
                AddMess(e, false, false, "A few stones seem to be riped of the ground.",
                    "May be putting them back will open the trapdoor.");
                AddMess(e, true, true, "Vous trouvez un trésor sous la trappe !");
                AddMess(e, false, true, "You find a treasure hiding beneath the trapdoor.");
                // Code 0005
                e = Add(5, EventTypeEnum.Enigma, floor: true, disable: true);
                AddMess(e, true, false, "Certaines dalles semblent détachées du sol.",
                    "Les replacer ouvrira peut être la porte ?");
                AddMess(e, false, false, "A few stones seem to be riped of the ground.",
                    "May be putting them back will open the trapdoor.");
                AddMess(e, true, true, "Vous trouvez un trésor sous la trappe !");
                AddMess(e, false, true, "You find a treasure hiding beneath the trapdoor.");
                // Code 0006
                e = Add(6, EventTypeEnum.Enigma, floor: true);
                AddMess(e, true, false, "Les trois leviers doivent être activés pour ouvrir la porte.");
                AddMess(e, false, false, "These 3 levers probably open the way down.");
                // Code 0007
                e = Add(7, EventTypeEnum.Enigma, transition: true, disable: true, final: true);
                AddMess(e, true, false, "Cette chose toutes choses dévore: Oiseaux, bêtes, arbres, fleurs.",
                    "Elle ronge le fer, mord l'acier, reduit les dures pierres en poudre,",
                    "met à mort les rois, détruit les villes et rabat les hautes montagnes.");
                AddMess(e, false, false, "This thing all things devours: birds, beasts, trees, flowers,",
                    "Molds iron, bites steel; Grinds hard stones to meal;",
                    "Slays king, ruins town, and beats the highest mountain down.");
                AddMess(e, true, true, "Bien vu ! Le trésor est là !");
                AddMess(e, false, true, "Well thought ! Treasure is here !");
                // Code 0008
                e = Add(8, EventTypeEnum.Enigma, transition: true, disable: true);
                AddMess(e, false, false, "Solve this puzzle to open the trapdoor.");
                // Code 0009
                e = Add(9, EventTypeEnum.Enigma, disable: true);
                AddMess(e, true, false, "Cette énigme doit ouvrir la trappe.",
                    "Combien de fois écrit on 9 en comptant de 1 à 100 ?");
                AddMess(e, false, false, "This riddle is the code to open the trapdoor.",
                    "How many 9 do you write when you count from 1 to 100");
                AddMess(e, true, true, "Vous avez maintenant accès à l'étage suivant.");
                AddMess(e, false, true, "You gained access to the next floor.");
                // Code 0010
                e = Add(10, EventTypeEnum.Enigma, transition: true, disable: true);
                AddMess(e, true, false, "Cette énigme doit ouvrir la trappe.",
                    "10 personnes serrent la main à chacune d'entre elle.",
                    "Combien de poignée de main sont échangées ?");
                AddMess(e, false, false, "This riddle is the code to open the trapdoor.",
                    "10 people meet and shake hands.", "How many handshakes took place?");
                AddMess(e, true, true, "Vous avez maintenant accès à l'étage suivant.");
                AddMess(e, false, true, "You gained access to the next floor.");
                // Code 0011
                e = Add(11, EventTypeEnum.Enigma, transition: true, disable: true);
                AddMess(e, true, false, "Cette énigme doit ouvrir la trappe.",
                    "Une cage contient des sangliers et des poules,","14 yeux et 20 pattes.",
                    "Combien de sanglier se trouvent dans la cage ?");
                AddMess(e, false, false, "This riddle is the code to open the trapdoor.",
                    "A cage contain boars and chicken,","14 eyes and 20 feet.", "How many chicken are in the cage?");
                AddMess(e, true, true, "Vous avez maintenant accès à l'étage suivant.");
                AddMess(e, false, true, "You gained access to the next floor.");
                // Code 0012
                e = Add(12, EventTypeEnum.Enigma, transition: true, disable: true);
                AddMess(e, false, false, "Solve this puzzle to open the trapdoor.");
                //todo Code 0013 
                //e = Add(13, EventTypeEnum.Enigma, transition: true);

                #endregion

                #region Mortal

                // Code 0014
                e = Add(14, EventTypeEnum.Mortal, locked: true);
                AddMess(e, true, false, "Du gas ! Vous retenez votre souffle, mais trop tard...");
                AddMess(e, false, false, "Poison gas ! You hold your breath, but too late...");

                // Code 0015
                e = Add(15, EventTypeEnum.Mortal, locked: true);
                AddMess(e, true, false, "A peine entré dans la salle, vous entendez un declic.",
                    "Une trappe s'ouvre sous vos pied, vous précipitant dans le vide.");
                AddMess(e, false, false, "The floor collapses beneath your feet.", "You fall into emptiness.");

                #endregion

                #region Jail 

                // Code 0016
                e = Add(16, EventTypeEnum.Jail, disable: true);
                AddMess(e, true, false, "Les murs se referment sur vous, vous n'avez plus d'issue...",
                    "Vous aurez besoin d'une aide extérieure pour vous en sortir !");
                AddMess(e, false, false, "The walls are closing in, no way out...",
                    "You'll need help from outside to open the doors!");
                // Code 0017
                e = Add(17, EventTypeEnum.Jail, disable: true);
                AddMess(e, true, false, "Le plafond, bardé de pieux descend vers vous...",
                    "Vous aurez besoin d'une aide extérieure pour vous en sortir !");
                AddMess(e, false, false, "Spiked ceiling descending, no way out...",
                    "You'll need help from outside to open the doors!");
                // Code 0018
                e = Add(18, EventTypeEnum.Jail, disable: true);
                AddMess(e, true, false, "De l'eau envahit la pièce, vous n'avez plus d'issue...",
                    "Vous aurez besoin d'une aide extérieure pour vous en sortir !");
                AddMess(e, false, false, "Water is floodind the room, no way out...",
                    "You'll need help from outside to open the doors!");
                // Code 0019
                e = Add(19, EventTypeEnum.Jail, disable: true);
                AddMess(e, true, false, "La température monte en flèche, et pas d'issue...",
                    "Si on ne vous aide pas de l'extérieur, vous allez rôtir !");
                AddMess(e, false, false, "It's getting really hot in here, and no way out...",
                    "You'll need help from outside to open the doors!");

                #endregion

                #region Qte

                // Code 0020
                e = Add(20, EventTypeEnum.Qte, transition: true, disable: true, qteTape: 20, final: true);
                AddMess(e, false, false, "A giant guardian stand in your way !", "The treasure must be close !");
                AddMess(e, true, true, "Prend ça !");
                AddMess(e, false, true, "That's how you do it ! Treasure is here !");

                // Code 0021
                e = Add(21, EventTypeEnum.Qte, qteTape: 4);
                AddMess(e, true, false, "Attention, cette pièce est piégée !", "Évitez les flèchettes empoisonnées !");
                AddMess(e, false, false, "Beware, it's a trap!", "Avoid the poison darts hurling toward you!");
                AddMess(e, true, true, "C'était moins une");
                AddMess(e, false, true, "Close one !");
                // Code 0022
                e = Add(22, EventTypeEnum.Qte, qteTape: 3, locked: true);
                AddMess(e, true, false, "Attention, cette pièce est piégée !",
                    "Une énorme boule de pierre se précipite sur vous !", "Sautez de côté pour l'esquiver !");
                AddMess(e, false, false, "Beware, it's a trap!", "A massive stone boulder is rolling toward you, run!");
                AddMess(e, true, true, "Ce n'est pas passez loin !");
                AddMess(e, false, true, "Nicely done !");
                // Code 0023
                e = Add(23, EventTypeEnum.Qte, qteTape: 3);
                AddMess(e, true, false, "Une Gargouille en pierre s'anime quand vous entrez !",
                    "Elle crache un jet d'acide sur vous, il faut esquiver !");
                AddMess(e, false, false, "A gargoyle awakes and spit acid at you!", "Duck, or burn!");
                AddMess(e, true, true, "C'était moins une");
                AddMess(e, false, true, "Close one !");
                // Code 0024
                e = Add(24, EventTypeEnum.Qte, qteTape: 4);
                AddMess(e, true, false, "De piques sortent du sol a votre passage.",
                    "Attention ou vous mettez les pieds.");
                AddMess(e, false, false, "Spikes pop out of the floor as you enter the room.", "Beware!");
                AddMess(e, true, true, "Ce n'est pas passez loin !");
                AddMess(e, false, true, "Close one !");
                // Code 0025
                e = Add(25, EventTypeEnum.Qte, qteTape: 4);
                AddMess(e, true, false, "Des haches fixées au plafond se balancent en rythme.",
                    "Vous devez avancer en cadence pour les esquiver !");
                AddMess(e, false, false, "Sweeping blades...", "You'll need quick reflexes to go through!");
                AddMess(e, true, true, "C'était moins une");
                AddMess(e, false, true, "Nicely done !");
                // Code 0026
                e = Add(26, EventTypeEnum.Qte, qteTape: 3, disable: true);
                AddMess(e, true, false, "Attention, cette pièce est piégée !",
                    "Une énorme boule de feu se précipite sur vous, sautez !");
                AddMess(e, false, false, "Beware, it's a trap!", "A HUGE fireball is coming to you!");
                AddMess(e, true, true, "Ce n'est pas passez loin !");
                AddMess(e, false, true, "Close one !");


                // Code 0027
                e = Add(27, EventTypeEnum.Qte, qteTape: 5, locked: true);
                AddMess(e, true, false, "Le plafond s'écroule sur vous, jettez vous vers la sortie !");
                AddMess(e, false, false, "The ceiling is collapsing on you! Roll out of danger!");
                AddMess(e, true, true, "Ce n'est pas passez loin !");
                AddMess(e, false, true, "Nicely done !");
                // Code 0028
                e = Add(28, EventTypeEnum.Qte, qteTape: 4);
                AddMess(e, true, false, "La pièce est remplie de lianes collantes.",
                    "Il va falloir les éviter pour passer à la pièce suivante.");
                AddMess(e, false, false, "This room is overwhelmed with sticky vines!",
                    "Dodge them to move to next room.");
                AddMess(e, true, true, "C'était moins une");
                AddMess(e, false, true, "Nicely done !");
                // Code 0029
                e = Add(29, EventTypeEnum.Qte, qteTape: 3);
                AddMess(e, true, false, "Vous marchez sur une dalle mouvante.",
                    "Sautez ou vous serez téléporté à l'entrée de l'étage !");
                AddMess(e, false, false, "You step on a pressure plate.",
                    "Dodge the trap or you'll be brought back to the floor entrance !");
                AddMess(e, true, true, "C'était moins une");
                AddMess(e, false, true, "Close one !");
                // Code 0030
                e = Add(30, EventTypeEnum.Qte, qteTape: 5);
                AddMess(e, true, false, "Des lames acérées menacent vos chevilles !",
                    "Sautez ou elles prendront vos pieds !");
                AddMess(e, false, false, "Blades are hurling toward your feets.", "Quick, dodge it!");
                AddMess(e, true, true, "Ce n'est pas passez loin !");
                AddMess(e, false, true, "Nicely done !");
                // Code 0031
                e = Add(31, EventTypeEnum.Qte, qteTape: 4);
                AddMess(e, true, false, "Des sables mouvants dans une pyramide ?",
                    "Ils sont fous ces mayas... Sortez de là !");
                AddMess(e, false, false, "Quick sands, in a pyramid?", "Those mayas... Get out of here!");
                AddMess(e, true, true, "C'était moins une");
                AddMess(e, false, true, "Nicely done !");
                // Code 0032
                e = Add(32, EventTypeEnum.Qte, qteTape: 4, disable: true);
                AddMess(e, true, false, "La salle regorge de plantes carnivores.",
                    "Défendez vous pour ne pas finir en engrais !");
                AddMess(e, false, false, "The room is full of carnivorous plant!",
                    "Defend yourself if you don't want to fertilize them");
                AddMess(e, true, true, "Vous avez détruit les plantes !");
                AddMess(e, false, true, "You destroyed the plants !");
                // Code 0033
                e = Add(33, EventTypeEnum.Qte, qteTape: 5, disable: true);
                AddMess(e, true, false, "Dans l'obscurité, vous distinguez une silhouette.", "C'est une momie !");
                AddMess(e, false, false, "In the darkness you see a little silhouette of man...", "It's a mummy!");
                AddMess(e, true, true, "Vous avez détruit la créature !");
                AddMess(e, false, true, "You destroyed the creature !");
                // Code 0034
                e = Add(34, EventTypeEnum.Qte, qteTape: 4, disable: true);
                AddMess(e, true, false, "Vous reconnaissez un collègue archéologue !",
                    "Lui n'a pas l'air content de vous voir...");
                AddMess(e, false, false, "You recognize a fellow archeologist!", "He doesn't seem pleased to see you...");
                AddMess(e, true, true, "Vous avez vaincu votre adversaire !");
                AddMess(e, false, true, "You vainquished !");
                // Code 0035
                e = Add(35, EventTypeEnum.Qte, qteTape: 4, disable: true);
                AddMess(e, true, false, "Des guerriers Mayas vous regardent hargneusement.",
                    "Les anciens locataires n'ont pas tous déménagé...");
                AddMess(e, false, false, "Mayas warriors fearlessly look at you.",
                    "Seems previous owner didn't move out after all");
                AddMess(e, true, true, "Vous avez vaincu vos adversaires !");
                AddMess(e, false, true, "You vainquished !");
                // Code 0036
                e = Add(36, EventTypeEnum.Qte, qteTape: 4, disable: true);
                AddMess(e, true, false, "Vous dérangez une goule qui se repaît goulument.", "Le repas c'est sacré.");
                AddMess(e, false, false, "You disturb a ghoul while it's eating.", "The meat was nice, you smell nicer");
                AddMess(e, true, true, "Vous avez détruit la créature !");
                AddMess(e, false, true, "You destroyed the creature !");
                // Code 0037
                e = Add(37, EventTypeEnum.Qte, qteTape: 3, disable: true);
                AddMess(e, true, false, "Des serpents ! Il fallait qu'il y ait des serpents !");
                AddMess(e, false, false, "Snakes! It had to be snakes!");
                AddMess(e, true, true, "Vous vous en êtes sortis !");
                AddMess(e, false, true, "You got out !");
                // Code 0038
                e = Add(38, EventTypeEnum.Qte, qteTape: 3, disable: true);
                AddMess(e, true, false, "Parce qu'on est tous un peu arachnophobes...");
                AddMess(e, false, false, "Spiders! No matter what people say, they're still scaring you!");
                AddMess(e, true, true, "Vous vous en êtes sortis !");
                AddMess(e, false, true, "You got out !");
                // Code 0039
                e = Add(39, EventTypeEnum.Qte, qteTape: 3);
                AddMess(e, true, false, "La salle bourdonne de vie insectoïde !", "Mieux vaut ne pas trainer !");
                AddMess(e, false, false, "The room is full of beatles, worms, locust...", "Better hurry up.");
                AddMess(e, true, true, "Vous vous en êtes sortis !");
                AddMess(e, false, true, "You got out !");

                /*
                // todo Code 0040
                e = Add(40, EventTypeEnum.Qte);
                // todo Code 0041
                e = Add(41, EventTypeEnum.Qte);
                // todo Code 0042
                e = Add(42, EventTypeEnum.Qte);
                // todo Code 0043
                e = Add(43, EventTypeEnum.Qte);
                // todo Code 0044
                e = Add(44, EventTypeEnum.Qte);
                // todo Code 0045
                e = Add(45, EventTypeEnum.Qte);
                */

                // Code 0046
                e = Add(46, EventTypeEnum.Qte);
                AddMess(e, true, false, "Un coffre attire votre attention.", "Oserez vous l'ouvrir ?");
                AddMess(e, false, false, "A treasure chest is sitting here.", "Will you dare open it?");

                #endregion

                #region Meeting

                // Code 0047
                e = Add(47, EventTypeEnum.Meeting);
                AddMess(e, true, false, "Un marchand Ici ?");
                AddMess(e, false, false, "What're ya sellin'?", "A merchant? Here???");
                // Code 0048
                e = Add(48, EventTypeEnum.Meeting, skip: true, disable: true);
                AddMess(e, true, false, "Une statuette dorée attire votre attention.");
                AddMess(e, false, false, "A golden statue catch your eye. Steal it?");
                AddMess(e, true, true, "A peine empoignée, un frisson vous parcoure...");
                AddMess(e, false, true, "When you get it, it sends shivers down your spine...");
                // Code 0049
                e = Add(49, EventTypeEnum.Meeting, skip: true, disable: true);
                AddMess(e, true, false, "Une statuette dorée attire votre attention");
                AddMess(e, false, false, "A golden statue catch your eye. Steal it?");
                AddMess(e, true, true, "A peine empoignée, vous vous sentez vivifié !");
                AddMess(e, false, true, "When you get it, you feel stronger and faster...");
                // Code 0050
                e = Add(50, EventTypeEnum.Meeting, disable: true);
                AddMess(e, true, false, "Un aventurier gît mort a vos pied.",
                    "La carte qu'il transporte ne lui sera plus utile.");
                AddMess(e, false, false, "A man is lying dead on the floor, a map in his hand.",
                    "He used to be an adventurer like you...");
                // Code 0051
                e = Add(51, EventTypeEnum.Meeting);
                AddMess(e, true, false, "Un touriste vous demande poliment son chemin.",
                    "Vous avez peut être un peu trop bu de vin hier.");
                AddMess(e, false, false, "A tourist ask you his way.", "May be you drinked too much yesterday...");
                // Code 0052
                e = Add(52, EventTypeEnum.Meeting, skip: true, disable: true);
                AddMess(e, true, false, "Un coffre attire votre attention.", "Oserez vous l'ouvrir ?");
                AddMess(e, false, false, "A treasure chest is sitting here.", "Will you dare open it?");

                /*
                // todo Code 0053
                e = Add(53, EventTypeEnum.Meeting);
                // todo Code 0054
                e = Add(54, EventTypeEnum.Meeting, locked: true);
                */

                #endregion

                #region Treasure

                // Code 0055
                e = Add(55, EventTypeEnum.Treasure, disable: true);
                AddMess(e, false, false, "\\o/ MONEY \\o/ MONEY \\o/");

                #endregion

                #region Entry

                // Code 0056
                e = Add(56, EventTypeEnum.Entry);
                AddMess(e, true, false, "Vous chauffez !");
                AddMess(e, false, false, "You're getting closer !");

                #endregion

                #region Empty

                e = Add(56, EventTypeEnum.Empty);
                AddMess(e, true, false, "[...]");
                AddMess(e, false, false, "[...]");

                #endregion
            }


            public EventRoom EmptyRoom
            {
                get { return _events.Select(r => r.Key).First(r => r.EventType == EventTypeEnum.Empty); }
            }

            public EventRoom EntryRoom
            {
                get { return _events.Select(r => r.Key).First(r => r.EventType == EventTypeEnum.Entry); }
            }

            public EventRoom TreasureRoom
            {
                get { return _events.Select(r => r.Key).First(r => r.EventType == EventTypeEnum.Treasure); }
            }

            public static EventRoomCollection Instance
            {
                get { return _instance ?? (_instance = new EventRoomCollection()); }
            }

            private EventRoom Add(byte index, EventTypeEnum type, bool final = false, bool floor = false,
                bool transition = false,
                bool skip = false, byte qteTape = 0, bool disable = false, bool locked = false)
            {
                var e = new EventRoom(index, type)
                {
                    Disable = disable,
                    Final = final,
                    Floor = floor,
                    Lock = locked,
                    QteTape = qteTape,
                    Skip = skip,
                    Transition = transition,
                };
                _events.Add(e, true);
                return e;
            }

            private void AddMess(EventRoom e, bool french, bool end, params string[] message)
            {
                if (end)
                    e.EndMessage.SetMessage(french ? LangEnum.French : LangEnum.English, message);
                else
                    e.StartMessage.SetMessage(french ? LangEnum.French : LangEnum.English, message);
            }


            public void ActiveEvent(EventRoom e)
            {
                if (_events.ContainsKey(e))
                    _events[e] = true;
            }

            public void DisableEvent(EventRoom e)
            {
                if (_events.ContainsKey(e))
                    _events[e] = false;
            }

            public EventRoom Get(EventTypeEnum type, bool active = true, bool? floor = null, bool? final = null, bool? transit = null)
            {
                IEnumerable<KeyValuePair<EventRoom, bool>> list =
                    _events.Where(e => e.Value == active && 
                        e.Key.EventType == type && 
                        ( floor == null || e.Key.Floor == floor) && 
                        ( final == null || e.Key.Final == final) && 
                        ( transit == null || e.Key.Transition == transit));
                if (!list.Any()) return EmptyRoom;
                return list.ElementAt(_random.Next(list.Count())).Key;
            }
        }
    }
}