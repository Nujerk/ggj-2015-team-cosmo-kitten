namespace Assets.Scripts.GenericBehavior.Enum
{
    public enum LocationEnum
    {
        Top,
        Left,
        Right,
        Bottom,
    }
}