namespace Assets.Scripts.GenericBehavior.Enum
{
    public enum EventTypeEnum
    {
        Qte,
        Meeting,
        Enigma,
        Jail,
        Mortal,
        Entry,
        Empty,
        Treasure,
    }
}