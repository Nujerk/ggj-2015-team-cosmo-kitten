﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;

public class RoomEngine : MonoBehaviour {

	public bool _isActive;


	private Sprite SpriteFloor {
		get { return GetComponent<Image>().sprite;}
	}

	public bool IsActive {
		get { return _isActive; }
		set { 
			_isActive = value;
			GetComponent<Image>().color = value ? Color.white : Color.gray;
		}
	}
	// Use this for initialization
	void Start () {
		if(!_isActive)
			IsActive = false;
	}

	// Update is called once per frame
	void Update() {
		if (Input.anyKeyDown && Game.s_FirstGroup.m_Room != Game.s_SecondGroup.m_Room) {
			IsActive = !IsActive;
		}
	}

}
