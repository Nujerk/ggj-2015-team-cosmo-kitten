﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UnitScript : MonoBehaviour {

	public 	int 	m_UnitId;
	private bool 	m_Initialized = false;
	private bool 	m_DeadState = false;
	private static UnitScript m_SelectedUnitScript;


	// Update is called once per frame
	void Update () {

		if (!m_Initialized) {
			RefreshContent();
			m_Initialized = true;
		}

		var l_Unit = Game.s_InitialGroup.m_Units [m_UnitId];
		if (!l_Unit.m_Alive && !m_DeadState) {
			m_DeadState = true;
		}
	}

	void RefreshContent(){
		transform.Find("UnitPicture").gameObject.GetComponent<Image> ().sprite = Unit.s_UnitPictures[m_DeadState ? m_UnitId + Unit.s_UnitPicturesDeadFlag : m_UnitId];
	}

	public void ClickUnit()
	{
		if (m_SelectedUnitScript == null) {
			m_SelectedUnitScript = this;
		} else {
			if(Game.s_InitialGroup.m_Units[m_UnitId].m_Group.m_Room != Game.s_InitialGroup.m_Units[m_SelectedUnitScript.m_UnitId].m_Group.m_Room)
			{
				Game.Notif("Les deux groupes ne sont\n pas dans la meme salle !");
				return;
			}
			var l_CurrentUnitId = m_UnitId;
			m_UnitId = m_SelectedUnitScript.m_UnitId;
			m_SelectedUnitScript.m_UnitId = l_CurrentUnitId;
			m_SelectedUnitScript.RefreshContent();
			m_SelectedUnitScript = null;
			RefreshContent();
		}
	}
}
