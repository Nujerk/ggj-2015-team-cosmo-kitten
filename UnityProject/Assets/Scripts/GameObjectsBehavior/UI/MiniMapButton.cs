﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.GenericBehavior.Entity;
using System.Linq;
using UnityEngine.UI;
using Assets.Scripts.GenericBehavior.Enum;

public class MiniMapButton : MonoBehaviour {


	static public Sprite ROOM_CURRENT 		= Resources.Load<Sprite>("button_actual");
	static public Sprite ROOM_UNKNOWN 		= Resources.Load<Sprite>("button_unseen");
	static public Sprite ROOM_ACCESSIBLE 	= Resources.Load<Sprite>("button_available");
	static public Sprite ROOM_VISITED 		= Resources.Load<Sprite>("button_visited");
	static public Sprite ROOM_SEALED 		= Resources.Load<Sprite>("button_visited_lock");

	public Room m_Room;

	// Update is called once per frame
	void Update () {
		Sprite l_ComputedImage = ROOM_UNKNOWN;

		// If room accessible
		var l_IsAccessible = false;

		if (m_Room.Rooms != null && m_Room.Rooms.Count > 0) {
			foreach (Room l_NearRoom in m_Room.Rooms.Values) {
				if (l_NearRoom == null)
					continue;

				l_IsAccessible = !l_NearRoom.IsActive;
				if (l_IsAccessible) {
					l_ComputedImage = ROOM_ACCESSIBLE;
					break;
				}
			}
		}

		// If room visited
		if (!m_Room.IsActive)
			l_ComputedImage = ROOM_VISITED;

		// If room sealed
		if (m_Room.Locked)
			l_ComputedImage = ROOM_SEALED;
	
		// If room current
		if (m_Room == Game.s_FirstGroup.m_Room || m_Room == Game.s_SecondGroup.m_Room)
			l_ComputedImage = ROOM_CURRENT;
		
		if (gameObject.GetComponent<Image> ().sprite != l_ComputedImage) {
			gameObject.GetComponent<Image> ().sprite = l_ComputedImage;
		}
	}

	public void ClickMiniMapRoom(){

	}
}
