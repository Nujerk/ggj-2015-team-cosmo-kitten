﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.GenericBehavior.Enum;

public class DoorScript : MonoBehaviour {

	static public void ClickDoorTop(){
		Game.s_CurrentGroup.MoveGroupToRoom(LocationEnum.Top);
	}
	
	static public void ClickDoorBottom(){
		Game.s_CurrentGroup.MoveGroupToRoom(LocationEnum.Bottom);
	}
	
	static public void ClickDoorLeft(){
		Game.s_CurrentGroup.MoveGroupToRoom(LocationEnum.Left);
	}
	
	static public void ClickDoorRight(){
		Game.s_CurrentGroup.MoveGroupToRoom(LocationEnum.Right);
	} 

}
